import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
import { AngularFireList,  AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class AppService {
    contactos: AngularFireList<any>;

    constructor(private db: AngularFireDatabase){}

    getContactos(){
        this.contactos = this.db.list('contactos') as AngularFireList <any[]>;
        return this.contactos;
    }
    getContactosFiltro(filtro: string){
        this.contactos = this.db.list('contactos', ref => ref.orderByChild('direccion').equalTo(filtro)
         ) as AngularFireList <any[]>;
        return this.contactos;
      }
}
