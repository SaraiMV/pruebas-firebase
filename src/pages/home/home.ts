import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { AngularFireList,  AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';
import { AppService } from '../../app/app.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})



export class HomePage implements OnInit {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  ciudades: string[];
  contactosRef: AngularFireList<any>;
  contactos: Observable<any[]>;

  constructor(private servicio: AppService ,private afAuth: AngularFireAuth, private toast: ToastController, public navCtrl: NavController,public db: AngularFireDatabase) {
    this.ciudades = ['Todos', 'Quito', 'Guayaquil', 'Riobamba'];
    this.itemsRef = db.list('contactos');
    this.items = this.itemsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
    
  }
  
  ngOnInit(){
    this.contactosRef = this.servicio.getContactos();
    this.contactos = this.contactosRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }

  onSelect(selectedValue: any) {
     if(selectedValue == "Todos")
    this.contactosRef = this.servicio.getContactos();
    else  
    this.contactosRef = this.servicio.getContactosFiltro(selectedValue);

    this.contactos = this.contactosRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  }


  ionViewWillLoad(){
    this.afAuth.authState.subscribe(data => {
      if(data && data.email && data.uid){
        this.toast.create({
        message: 'Welcome , ${data.email}',
        duration: 3000
      }).present();
    }else{
      this.toast.create({
        message: 'No se encontraron datos',
        duration: 3000
      }).present();
    }
    });
  }

  addItem(newName: string) {
    this.itemsRef.push({ nombre: newName });
  }
  updateItem(key: string, newText: string) {
    this.itemsRef.update(key, { nombre: newText });
  }
  deleteItem(key: string) {    
    this.itemsRef.remove(key); 
  }
  deleteEverything() {
    this.itemsRef.remove();
  }

  
}
